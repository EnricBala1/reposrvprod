package com.techuniversity.prod.controllers;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/productos")
public class ProductoController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(@RequestParam(defaultValue =  "-1") String page) {
        int iPage = Integer.parseInt(page);
        if (iPage == -1) {
            return productoService.findAll();
        } else {
            return productoService.findPaginado(iPage);
        }
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable String id) {

        return productoService.findByid(id);
    }
    
    @PostMapping("/productos")
    public ResponseEntity<ProductoModel> insertProducto(@RequestBody ProductoModel nuevo) {
        productoService.save(nuevo);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(nuevo.getId())
                .toUri();
        return ResponseEntity.created(location).build();
        //return nuevo;
    }

    @PutMapping("/productos")
    public void updateProductos(@RequestBody ProductoModel producto) {
        productoService.save(producto);
    }

    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody ProductoModel producto) {
        return productoService.deleteProducto(producto);
    }

}
